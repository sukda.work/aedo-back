// userRecordModel.js

var mongoose = require("mongoose");

var userRecordSchema = mongoose.Schema(
  {
    title: {
      type: String
    },
    subTitle: {
      type: String
    },
    createTime: {
      type: Date
    },
    userOnline: {
      type: Number
    },
    userEnrolled: {
      type: Number
    },
    userComplete: {
      type: Number
    },
    userPosted: {
      type: Number
    },
    userSharing: {
      type: Number
    },
    userIncome: {
      type: Number
    }
  },
  {
    collection: "item"
  }
);

var userRecord = mongoose.model("item", userRecordSchema);
module.exports = userRecord;